# Analyze Java source code for Spring annotations

## Checks

Is the annotation in the spring package `org.springframework`

- Class Level

- Class properties static and non  

- Class methods static and non-static

- Method arguments
