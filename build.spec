version: 0-1

phases:
  pre_build:
    commands:
      - echo Hello
      - echo fetching cod3

  build:
    commands:
      - echo starting build: `date`
      - mvn package

  post_build:
    commands:
      - echo Done
      - ls -l target/java-spring-scan.jar
