package jsc;


import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import com.github.javaparser.utils.CodeGenerationUtils;
import com.github.javaparser.utils.SourceRoot;

import java.nio.file.Paths;


public class Scan {

    public static void main(String[] args) {
        Scan scan = new Scan();
        scan.scanSource(args);
    }

    private void scanSource(String[] args) {
        for (String path : args) {

            // SourceRoot is a tool that read and writes Java files from packages on a certain root directory.
            // In this case the root directory is found by taking the root from the current Maven module,
            // with src/main/resources appended.
            SourceRoot sourceRoot = new SourceRoot(

            for (CompilationUnit cu : sourceRoot.getCompilationUnits()) {

                cu.accept(new ModifierVisitor<Void>() {
                    /**
                     * For every if-statement, see if it has a comparison using "!=".
                     * Change it to "==" and switch the "then" and "else" statements around.
                     */
                    @Override
                    public Visitable visit(IfStmt n, Void arg) {
                        // Figure out what to get and what to cast simply by looking at the AST in a debugger!
                        n.getCondition().ifBinaryExpr(binaryExpr -> {
                            if (binaryExpr.getOperator() == BinaryExpr.Operator.NOT_EQUALS && n.getElseStmt()
                                                                                               .isPresent()) {
                        /* It's a good idea to clone nodes that you move around.
                            JavaParser (or you) might get confused about who their parent is!
                        */
                                Statement thenStmt = n.getThenStmt().clone();
                                Statement elseStmt = n.getElseStmt().get().clone();
                                n.setThenStmt(elseStmt);
                                n.setElseStmt(thenStmt);
                                binaryExpr.setOperator(BinaryExpr.Operator.EQUALS);
                            }
                        });
                        return super.visit(n, arg);
                    }
                }, null);
            }

            // This saves all the files we just read to an output directory.
            sourceRoot.saveAll(CodeGenerationUtils.mavenModuleRoot(Scan.class)
                                                  // appended with a path to "output"
                                                  .resolve(Paths.get("output")));
        }
    }
}

